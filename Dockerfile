FROM registry.gitlab.com/archsible/archlinux-systemd-docker-test:latest

RUN sudo pacman -Sy && \
	sudo pacman -S --noconfirm wget expat git jq && \
	sudo pacman -Scc --noconfirm
RUN sudo pacman -Sy && \
	mkdir -p /tmp/pkgbuilds && \
	cd /tmp/pkgbuilds && \
	wget https://aur.archlinux.org/cgit/aur.git/snapshot/auracle-git.tar.gz && \
	wget https://aur.archlinux.org/cgit/aur.git/snapshot/pacaur.tar.gz && \
	wget https://aur.archlinux.org/cgit/aur.git/snapshot/pacget.tar.gz && \
	wget https://aur.archlinux.org/cgit/aur.git/snapshot/yay.tar.gz && \
	tar xzf auracle-git.tar.gz && \
	tar xzf pacaur.tar.gz && \
	tar xzf pacget.tar.gz && \
	tar xzf yay.tar.gz && \
	cd auracle-git && \
	makepkg -s -r -i -c --noconfirm && \
	cd ../pacaur && \
	makepkg -s -r -i -c --noconfirm && \
	cd ../pacget && \
	makepkg -s -r -i -c --noconfirm && \
	cd ../yay && \
	makepkg -s -r -i -c --noconfirm && \
	rm -rf /tmp/pkgbuilds && \
	sudo pacman -Scc --noconfirm



